﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralizedImportTool.Model
{
    class DataGridTemplate
    {
        public int SelectedIndex { get; set; }
        public string ColumnIndex { get; set; }
    }
}
