﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralizedImportTool
{
    public static class EnumClass
    {
        public enum CompListColumnHeader
        {
            SelectColumn,
            PartNumber,
            ManufacturerName,
            ManufacturerNumber,
        };
    }
}
