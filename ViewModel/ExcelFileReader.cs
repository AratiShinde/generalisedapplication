﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.Office.Interop.Excel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Application = Microsoft.Office.Interop.Excel.Application;
using DataTable = System.Data.DataTable;
using Sheets = DocumentFormat.OpenXml.Spreadsheet.Sheets;
using Workbook = Microsoft.Office.Interop.Excel.Workbook;
using Worksheet = Microsoft.Office.Interop.Excel.Worksheet;

namespace GeneralizedImportTool.ViewModel
{
    public class ExcelFileReader
    {
        readonly string excelFilepath;       
        
        public ExcelFileReader(string excelFilePath)
        {
            try
            {               
                excelFilepath = excelFilePath;                
            }
            catch (Exception)
            {
                MessageBox.Show("File Not Found");
            }
        }       
        public DataTable GetTable()
        {
            DataTable dt = new DataTable();
            try
            {
                //for (colCnt = 1; colCnt <= excelRange.Columns.Count; colCnt++)
                //{
                //    string strColumn = "";
                //    strColumn = (string)(excelRange.Cells[1, colCnt] as Microsoft.Office.Interop.Excel.Range).Value2;
                //    dt.Columns.Add(strColumn, typeof(string));
                //}
                //for (rowCnt = 1; rowCnt <= excelRange.Rows.Count; rowCnt++)
                //{
                //    string strData = "";
                //    for (colCnt = 1; colCnt <= excelRange.Columns.Count; colCnt++)
                //    {
                //        try
                //        {
                //            strCellData = (string)(excelRange.Cells[rowCnt, colCnt] as Microsoft.Office.Interop.Excel.Range).Value2;
                //            strData += strCellData + "|";
                //        }
                //        catch (Exception)
                //        {
                //            douCellData = (excelRange.Cells[rowCnt, colCnt] as Microsoft.Office.Interop.Excel.Range).Value2;
                //            strData += douCellData.ToString() + "|";
                //        }
                //    }
                //    strData = strData.Remove(strData.Length - 1, 1);
                //    dt.Rows.Add(strData.Split('|'));
                //}
                //excelBook.Close(true, null, null);
                //excelApp.Quit();
                //return dt;
                //-------------------------------------------------//

                using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(excelFilepath, false))
                {

                    WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                    IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<DocumentFormat.OpenXml.Spreadsheet.Sheets>().Elements<Sheet>();
                    string relationshipId = sheets.First().Id.Value;
                    WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                    DocumentFormat.OpenXml.Spreadsheet.Worksheet workSheet = worksheetPart.Worksheet;
                    SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                    IEnumerable<Row> rows = sheetData.Descendants<Row>();

                    foreach (Cell cell in rows.ElementAt(0))
                    {
                        dt.Columns.Add(GetCellValue(spreadSheetDocument, cell));
                    }

                    foreach (Row row in rows) //this will also include your header row...
                    {
                        DataRow tempRow = dt.NewRow();

                        for (int i = 0; i < row.Descendants<Cell>().Count(); i++)
                        {
                            //tempRow[i] = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(i));
                            Cell cell = row.Descendants<Cell>().ElementAt(i);
                            int actualCellIndex = CellReferenceToIndex(cell);
                            tempRow[actualCellIndex] = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(i));
                        }

                        dt.Rows.Add(tempRow);
                    }

                }             
            }
            catch (Exception e)
            {                
                MessageBox.Show(e.Message.ToString()); 
            }
            return dt;
        }
        public static string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
            string value = cell.CellValue?.InnerXml;

            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
            {
                return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
            }
            else
            {
                return value;
            }
        }
        private static int CellReferenceToIndex(Cell cell)
        {
            int index = 0;
            string reference = cell.CellReference.ToString().ToUpper();
            foreach (char ch in reference)
            {
                if (Char.IsLetter(ch))
                {
                    int value = (int)ch - (int)'A';
                    index = (index == 0) ? value : ((index + 1) * 26) + value;
                }
                else
                    return index;
            }
            return index;
        }
    }
}
