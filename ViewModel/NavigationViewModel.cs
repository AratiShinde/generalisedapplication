﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GeneralizedImportTool.ViewModel
{
    public class NavigationViewModel
    {
        public Type ViewModelType { get; private set; }

        public NavigationViewModel(Type viewModelType)
        {
            ViewModelType = viewModelType;
        }

    }
}
