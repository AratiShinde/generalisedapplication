﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using Microsoft.Office.Interop.Excel;
using Prism.Commands;
using GeneralizedImportTool.View.PrepaireMappingTemplates;
using System.ComponentModel;
using System.Text.RegularExpressions;
using DataTable = System.Data.DataTable;
using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Command;
using System.Windows;
using GalaSoft.MvvmLight;
using DocumentFormat.OpenXml.Packaging;
using S = DocumentFormat.OpenXml.Spreadsheet.Sheets;
using E = DocumentFormat.OpenXml.OpenXmlElement;
using A = DocumentFormat.OpenXml.OpenXmlAttribute;

namespace GeneralizedImportTool.ViewModel
{
    public class UploadFileViewModel : ViewModelBase1
    {

        DataTable records;

        string fileName;

        string filePath;

        List<string> sheets;

        string selectedWorkbook;

        string errorMessage;
        public IMessageBoxService MessageBoxService { get; set; }
        public DelegateCommand ShowSecondViewCommand { get; private set; }
        public DelegateCommand UploadFileCommand { get; set; }
        public UploadFileViewModel()
        {
            ShowSecondViewCommand = new DelegateCommand(ShowSecondView);
            UploadFileCommand = new DelegateCommand(SelectfileToUpload);
            LoadSelections();
        }
        private void ShowSecondView()
        {
            if (IsValid())
            {
                ComponentListMappingViewModel objMapping = new ComponentListMappingViewModel(FilePath, FileName, SelectedWorkBook);
                Messenger.Default.Send(new ChangePage(typeof(ComponentListMappingViewModel), objMapping));
            }
        }
        public string FileName
        {
            get { return fileName; }
            set
            {
                fileName = value;
                RaisePropertyChanged("FileName");                
            }
        }
        public string FilePath
        {
            get { return filePath; }
            set
            {
                filePath = value;
                RaisePropertyChanged("Filepath");
                GetExcelSheetNames(FilePath);
            }
        }
        public List<string> Sheets
        {
            get { return sheets; }
            set
            {
                sheets = value;
                RaisePropertyChanged("Sheets");
            }
        }
        public string SelectedWorkBook
        {
            get { return selectedWorkbook; }
            set
            {
                selectedWorkbook = value;
                RaisePropertyChanged("RaisePropertyChanged");
            }
        }
        public string ErrorMessage
        {
            get { return errorMessage; }
            set
            {
                errorMessage = value;
                RaisePropertyChanged("ErrorMessage");
            }
        }
        public DataTable Records
        {
            get { return records; }
            set
            {
                records = value;
                RaisePropertyChanged("ErrorMessage");
            }
        }
        void SelectfileToUpload()
        {
            OpenFileDialog result = new OpenFileDialog();
            this.Sheets = new List<string>();
            result.Multiselect = false;
            result.Filter = "Excel Files|*.xls;*.xlsx;*.xlsm";
            if (result.ShowDialog() == true)
            {
                FilePath = result.FileName;
                FileName = result.SafeFileName;
                foreach (string sheetname in GetExcelSheetNames(FilePath))
                {
                    this.Sheets.Add(sheetname); RaisePropertyChanged("Sheets");
                }
                SaveSelections();
            }
        }
        public bool IsValid()
        {
            var valid = true;
            if (string.IsNullOrWhiteSpace(FileName))
            {
                ErrorMessage = "Please select file to upload.";
                valid = false;
                return valid;
            }
            else if (string.IsNullOrWhiteSpace(SelectedWorkBook))
            {
                ErrorMessage = "Please select sheet name.";
                valid = false;
                return valid;
            }
            return valid;
        }
        public static IEnumerable<string> GetExcelSheetNames(string excelFilePath)
        {
            String[] excelSheets = null;
            try
            {                
                // Open file as read-only.
                using (SpreadsheetDocument mySpreadsheet = SpreadsheetDocument.Open(excelFilePath, false))
                {
                    S sheets = mySpreadsheet.WorkbookPart.Workbook.Sheets;
                    excelSheets = new string[sheets.Count()];
                    int i = 0;
                    // For each sheet, display the sheet information.
                    foreach (E sheet in sheets)
                    {
                        
                        foreach (A attr in sheet.GetAttributes())
                        {
                            if (attr.Value != null)
                            {
                                excelSheets[i] = attr.Value;
                                i++;
                                break;
                            }
                        }
                    }
                }
            }
            catch(Exception)
            {
                MessageBox.Show("File Not Found");
            }
            return excelSheets;
        }
        void LoadSelections()
        {
            if(!string.IsNullOrWhiteSpace(Properties.Settings.Default.ComponentListFilePath))
            {
                FilePath = Properties.Settings.Default.ComponentListFilePath;
                FileName = Properties.Settings.Default.ComponentListFileName;
                this.Sheets = new List<string>();
                foreach (string sheetname in GetExcelSheetNames(FilePath))
                {
                    this.Sheets.Add(sheetname); RaisePropertyChanged("Sheets");
                }
            }            
        }
        void SaveSelections()
        {
            Properties.Settings.Default.ComponentListFilePath = FilePath;
            Properties.Settings.Default.ComponentListFileName = FileName;
            Properties.Settings.Default.Save();
        }
    }
}
