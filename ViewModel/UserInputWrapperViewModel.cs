﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using GeneralizedImportTool.View.PrepaireMappingTemplates;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace GeneralizedImportTool.ViewModel
{
    class UserInputWrapperViewModel : ViewModelBase
    {
        UploadFileViewModel objUploadFile;
        ComponentListMappingViewModel objComplist;
        ViewModelBase currentViewModel;
        public RelayCommand ShowFirstViewCommand { get; private set; }
        public RelayCommand ShowSecondViewCommand { get; private set; }

        public UserInputWrapperViewModel(UploadFileViewModel firstViewModel, ComponentListMappingViewModel secondViewModel)
        {
            //NavCommand = new RelayCommand<string>(OnNavigation);
            //objUploadFile = new UploadFile();
            //OnNavigation("UploadComponent");
            objUploadFile = firstViewModel;
            objComplist = secondViewModel;
           
        ShowFirstViewCommand = new RelayCommand(ShowFirstView);
        ShowSecondViewCommand = new RelayCommand(ShowSecondView);        
        ShowFirstView();
        Messenger.Default.Register<ChangePage>(this, ChangePage);
        }
        public ViewModelBase CurrentViewMode
        {
            get { return currentViewModel; }
            set { SetProperty(ref currentViewModel, value); }
        }

        //public RelayCommand<string> NavCommand { get; private set; }
        //private void OnNavigation(string destination)
        //{
        //    switch (destination)
        //    {
        //        case "UploadComponent":

        //            CurrentViewMode = objUploadFile;
        //            break;
        //        case "OutputTemplate":
        //            //objCompMapping = new View.PrepaireMappingTemplates.ComponentListMapping();
        //            break;
        //        case "MappingTemplate":
        //            break;
        //        default:
        //            objUploadFile = new View.PrepaireMappingTemplates.UploadFile();
        //            CurrentViewMode = objUploadFile;
        //            break;

        //    }

        //}
        private void ShowFirstView()
        {
            CurrentViewMode = objUploadFile;
        }

        private void ShowSecondView()
        {
            CurrentViewMode = objComplist;
        }
        private void ChangePage(ChangePage message)
        {
            if (message.ViewModelType == typeof(UploadFile))
            {
                ShowFirstView();
            }
            else if (message.ViewModelType == typeof(ComponentListMapping))
            {
                ShowSecondView();
            }

        }


    }
}
