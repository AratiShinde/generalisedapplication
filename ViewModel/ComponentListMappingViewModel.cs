﻿using GalaSoft.MvvmLight;
using GeneralizedImportTool.Model;
using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using static GeneralizedImportTool.EnumClass;
namespace GeneralizedImportTool.ViewModel
{
    public class ComponentListMappingViewModel : ViewModelBase1
    {
        string filePath;

        string fileName;

        string selectedWorkBook;

        DataTable records;

        string errorMessage;

        int combSelectedIndex;

        public DelegateCommand<UIElement> ValidateColumnCommand { get; set; }

        private List<DataGridTemplate> SelectedColumn = new List<DataGridTemplate>();
        public string FileName
        {
            get { return fileName; }
            set { fileName = value; RaisePropertyChanged("FileName"); }
        }
        public string FilePath
        {
            get { return filePath; }
            set { filePath = value; RaisePropertyChanged("FilePath"); }
        }
        public string SelectedWorkBook
        {
            get { return selectedWorkBook; }
            set { selectedWorkBook = value; RaisePropertyChanged("SelectedWorkBook"); }
        }
        public DataTable Records
        {
            get { return records; }
            set { records = value; RaisePropertyChanged("Records"); }
        }
        public string ErrorMessage
        {
            get { return errorMessage; }
            set { errorMessage = value; RaisePropertyChanged("ErrorMessage"); }
        }
        public int CombSelectedIndex
        {
            get { return combSelectedIndex; }
            set
            {
                if (combSelectedIndex != value)
                {
                    combSelectedIndex = value;
                    RaisePropertyChanged("CombSelectedIndex");
                }
            }
        }
        public ComponentListMappingViewModel() { }
        public ComponentListMappingViewModel(string FilePath1, string FileName1, string SelecedWorkBook1)
        {
            FilePath = FilePath1;
            FileName = FileName1;
            SelectedWorkBook = SelecedWorkBook1;
            CombSelectedIndex = 0;
            ExcelFileReader objExcelFileReader = new ExcelFileReader(FilePath);
            Records = objExcelFileReader.GetTable();
            ValidateColumnCommand = new DelegateCommand<UIElement>(ValidateColumn);
        }
        void ValidateColumn(UIElement obj)
        {
            try
            {
                ComboBox columnHeader = (ComboBox)obj;
                if (columnHeader?.DataContext != null && columnHeader?.SelectedIndex != null && columnHeader?.SelectedIndex > 0)
                {
                    var columnIndex = columnHeader.SelectedIndex;
                    string newColumnNam = ((CompListColumnHeader)columnIndex).ToString();
                    bool case1 = SelectedColumn.Any(x => x.SelectedIndex == columnHeader.SelectedIndex);
                    bool case2 = SelectedColumn.Any(x => x.ColumnIndex == columnHeader.DataContext.ToString());
                    bool case3 = SelectedColumn.Any(x => x.SelectedIndex == columnHeader.SelectedIndex && x.ColumnIndex != columnHeader.DataContext.ToString());

                    if (case1)
                    {
                        if (case3)
                        {
                            DataGridTemplate newobjDataTemplate = new DataGridTemplate
                            {
                                SelectedIndex = columnIndex,
                                ColumnIndex = columnHeader.DataContext.ToString(),
                            };
                            SelectedColumn.Add(newobjDataTemplate);
                        }
                        CombSelectedIndex = 0;
                        ErrorMessage = "Column " + newColumnNam + " is already used by another column.Please select a unique column.";
                    }
                    else
                    {
                        if (case2)
                        {
                            List<DataGridTemplate> oldobjDataTemplate = SelectedColumn.FindAll(y => y.ColumnIndex == columnHeader.DataContext.ToString()).ToList();
                            foreach (DataGridTemplate iteration in oldobjDataTemplate)
                            {
                                int index1 = SelectedColumn.FindIndex(x => x.ColumnIndex == iteration.ColumnIndex.ToString() && x.SelectedIndex == iteration.SelectedIndex);
                                SelectedColumn.RemoveAt(index1);
                            }
                            DataGridTemplate newobjDataTemplate = new DataGridTemplate
                            {
                                SelectedIndex = columnIndex,
                                ColumnIndex = columnHeader.DataContext.ToString(),
                            };
                            SelectedColumn.Add(newobjDataTemplate);
                            CombSelectedIndex = columnIndex;
                            ErrorMessage = "";
                        }
                        else
                        {
                            DataGridTemplate objDataTemplate = new DataGridTemplate
                            {
                                SelectedIndex = columnIndex,
                                ColumnIndex = columnHeader.DataContext.ToString(),
                            };
                            CombSelectedIndex = columnIndex;
                            SelectedColumn.Add(objDataTemplate);
                            ErrorMessage = "";
                        }

                    }

                }
                else if (columnHeader?.SelectedIndex == 0)
                {
                    List<DataGridTemplate> oldobjDataTemplate = SelectedColumn.FindAll(y => y.ColumnIndex == columnHeader.DataContext.ToString()).ToList();
                    foreach (DataGridTemplate iteration in oldobjDataTemplate)
                    {
                        int index1 = SelectedColumn.FindIndex(x => x.ColumnIndex == iteration.ColumnIndex.ToString() && x.SelectedIndex == iteration.SelectedIndex);
                        SelectedColumn.RemoveAt(index1);
                    }
                    ErrorMessage = "";
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Invalid Column mapping");
            }
        }
    }
}
