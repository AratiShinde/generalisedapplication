﻿using GeneralizedImportTool.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneralizedImportTool
{
    class ChangePage
    {
        public Type ViewModelType { get; private set; }
        public ViewModelBase1 objViewModelType;

        public ChangePage(Type viewModelType)
        {
            ViewModelType = viewModelType;
        }
        public ChangePage(Type viewModelType, ViewModelBase1 objBase)
        {
            ViewModelType = viewModelType;
            objViewModelType = new ViewModelBase1();
            objViewModelType=objBase;

        }
    }
}
