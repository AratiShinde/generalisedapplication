﻿using GalaSoft.MvvmLight;
using GeneralizedImportTool.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GeneralizedImportTool.View.PrepaireMappingTemplates
{
    /// <summary>
    /// Interaction logic for UserInputWrapper.xaml
    /// </summary>
    public partial class UserInputWrapper : UserControl
    {

        UserInputWrapperViewModel objUserIpWrap;        
        public UserInputWrapper(UploadFileViewModel obj1,ComponentListMappingViewModel obj2)
        {
            InitializeComponent();           
            objUserIpWrap = new UserInputWrapperViewModel(obj1, obj2);
            this.DataContext = objUserIpWrap;
        }
    }
}
