﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using GeneralizedImportTool.View.PrepaireMappingTemplates;
using GeneralizedImportTool.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace GeneralizedImportTool
{
    public class MainWindowViewModel : ViewModelBase1
    {

        private ViewModelBase1 _currentViewModel;

        private UserControl selectedModel;

        UploadFile obj1 = new UploadFile();

        public RelayCommand ShowFirstViewCommand { get; private set; }
        public RelayCommand ShowSecondViewCommand { get; private set; }

        private readonly UploadFileViewModel firstViewModel;

        public ViewModelBase1 CurrentViewModel
        {
            get { return _currentViewModel; }
            set { _currentViewModel = value; RaisePropertyChanged("CurrentViewModel"); }
        }
        public UserControl SelectedModel
        {
            get { return selectedModel; }
            set { selectedModel = value; RaisePropertyChanged("SelectedModel"); }
        }

        [Obsolete("Only for design data", true)]
        public MainWindowViewModel() : this(new UploadFileViewModel(), null)
        {
        }
        public MainWindowViewModel(UploadFileViewModel FirstViewModel, ComponentListMappingViewModel SecondViewModel)
        {
            firstViewModel = FirstViewModel;
            ShowFirstViewCommand = new RelayCommand(ShowFirstView);
            ShowFirstView();
            Messenger.Default.Register<ChangePage>(this, ChangePage);
        }
        private void ShowFirstView()
        {
            CurrentViewModel = firstViewModel;

        }
        private void ChangePage(ChangePage message)
        {
            if (message.ViewModelType == typeof(UploadFileViewModel))
            {
                ShowFirstView();
            }
            else if (message.ViewModelType == typeof(ComponentListMappingViewModel))
            {
                CurrentViewModel = message.objViewModelType;
            }
        }
    }
}
